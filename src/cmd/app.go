package main

import (
	"context"
	"crypto/tls"
	"flag"
	"os"
	"runtime"

	"github.com/google/gops/agent"

	// Resource TCP Client Dep
	auth "bitbucket.org/shipperid/sdk-go/stdlib/auth"
	fflag "bitbucket.org/shipperid/sdk-go/stdlib/fflag"
	httpclient "bitbucket.org/shipperid/sdk-go/stdlib/httpclient"
	notifier "bitbucket.org/shipperid/sdk-go/stdlib/notifier"

	// Resource Storage Dep
	cqlx "bitbucket.org/shipperid/sdk-go/stdlib/cql"
	elastic "bitbucket.org/shipperid/sdk-go/stdlib/elastic"
	mongo "bitbucket.org/shipperid/sdk-go/stdlib/mongo"
	redis "bitbucket.org/shipperid/sdk-go/stdlib/redis"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"

	// Resource Event PubSub Dep
	kafkalib "bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	orch "bitbucket.org/shipperid/sdk-go/stdlib/orch"

	// Server Handlers Dep
	// bpmhandler "bitbucket.org/shipperid/getting-started/src/handler/bpm"
	// pubsubhandler "bitbucket.org/shipperid/getting-started/src/handler/pubsub"
	resthandler "bitbucket.org/shipperid/getting-started/src/handler/rest"

	// Server Infrastructure Dep
	cfg "bitbucket.org/shipperid/sdk-go/stdlib/config"
	errors "bitbucket.org/shipperid/sdk-go/stdlib/errors"
	grace "bitbucket.org/shipperid/sdk-go/stdlib/grace"
	health "bitbucket.org/shipperid/sdk-go/stdlib/health"
	httpmiddleware "bitbucket.org/shipperid/sdk-go/stdlib/httpmiddleware"
	httpmux "bitbucket.org/shipperid/sdk-go/stdlib/httpmux"
	httpserver "bitbucket.org/shipperid/sdk-go/stdlib/httpserver"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	parser "bitbucket.org/shipperid/sdk-go/stdlib/parser"
	telemetry "bitbucket.org/shipperid/sdk-go/stdlib/telemetry"

	// Business Layer Dep
	domain "bitbucket.org/shipperid/getting-started/src/business/domain"
	usecase "bitbucket.org/shipperid/getting-started/src/business/usecase"
)

var (
	// Flag Declaration
	staticConfPath              string
	restartOnStaticConfigChange bool
	minJitter                   int
	maxJitter                   int

	// Config Vars Declaration
	conf        Conf
	runtimeConf Conf
	secrets     Conf

	// Resource - TCP Client
	featureFlag fflag.FFlag
	httpClient  httpclient.HTTPClient
	notif       notifier.Notifier

	// Resource - Storage
	cqlClient0     cqlx.CQL
	elasticClient0 elastic.Elastic
	mongoClient0   mongo.Mongo
	redisClient0   redis.Redis
	sqlClient0     sqlx.SQL

	// Resource - Event PubSub
	kafkaProd kafkalib.Producer

	// Resource - BPM Engine
	bpmProd orch.Producer

	// Server Dep - Handler
	kafkaCons kafkalib.Consumer
	bpmCons   orch.Consumer

	// Server Handlers
	rest resthandler.REST
	// pubsub pubsubhandler.PubSub
	// bpm    bpmhandler.BPM

	// Server Infrastructure
	logger     log.Logger
	staticConf cfg.Conf
	remoteConf cfg.Conf
	secretConf cfg.Conf
	tele       telemetry.Telemetry
	healt      health.Health
	parse      parser.Parser
	httpMware  httpmiddleware.HttpMiddleware
	httpMux    httpmux.HttpMux
	httpServer httpserver.HTTPServer
	app        grace.App
	aut        auth.Auth

	// Business Layer
	dom *domain.Domain
	uc  *usecase.Usecase
)

func init() {
	if err := agent.Listen(agent.Options{
		ShutdownCleanup: true,
	}); err != nil {
		os.Exit(1)
	}

	// Init Flag Settings
	// get static config file path
	flag.StringVar(&staticConfPath, "staticConfPath", "./etc/conf/development.yaml", "config path")
	// restart on static config change : disable this if you deploy this app within kubernetes environment
	// enable this within k8s environment can distort the k8s pods lifecycle since every change in configmap will
	// trigger pod restarts.
	flag.BoolVar(&restartOnStaticConfigChange, "restartOnStaticConfigChange", true, "restart on static config change")
	// declare on minimum sleep during app initialization
	// if min < 0 then DefaultMinJitter will be applied
	flag.IntVar(&minJitter, "minSleep", DefaultMinJitter, "min. sleep duration during app initialization")
	// declare on maximum sleep during app initialization
	// if max < min then DefaultMaxJitter will be applied
	// see SleepWithJitter
	flag.IntVar(&maxJitter, "maxSleep", DefaultMaxJitter, "max. sleep duration during app initialization")
	// Parse All Flags
	flag.Parse()

	// Add Sleep with Jitter to drag the the initialization time among instances
	SleepWithJitter(minJitter, maxJitter)

	// Logger Initialization
	// Logger is initialized with default options during apps initialization
	// as it should be overridden after reading the config
	// 	output : stdout
	//	level : trace
	// 	format : txt
	logger = log.Init(log.Options{})

	// Initialize Static Config
	// K8s environment mount this path as volume to get config hot reload
	// Disable the hot reload from Apps on Static Config Change, see flag : restartOnStaticConfigChange
	staticConf = cfg.Init(logger, cfg.AppStaticConfig, cfg.Options{
		// Not applicable for static configuration options
		// Provider : "",
		// Host : "",
		// Enabled : true, // always true
		// GPGKeyRing : "",
		// RemoteWatchPeriod : 0s,
		Type:            `yaml`, // always use yaml for static conf
		Path:            staticConfPath,
		RestartOnChange: restartOnStaticConfigChange,
	})
	staticConf.ReadAndWatch(&conf)

	// Override All Config : Static
	// Swagger config is initialized
	// Injected build variable is initialized
	OverrideStaticConfig(staticConf, &conf)

	// Initialize Remote Config (App Runtime Config)
	// Remote Config should be used only to store run time config
	// Remote Config will try to connect during app initialization with retry backoff mechanism
	// It will block all the operations until success
	// App is forced to exit if the remote configuration fails
	// Secret Config will override the config
	remoteConf = cfg.Init(logger, cfg.AppRemoteConfig, cfg.Options{
		Enabled:  conf.RemoteKV.Enabled,
		Type:     conf.RemoteKV.Type,
		Provider: conf.RemoteKV.Provider,
		Host:     conf.RemoteKV.Host,
		// follow standard way of the url config path to help organizing the kv store
		Path:              conf.RemoteKV.Path,
		RemoteWatchPeriod: conf.RemoteKV.RemoteWatchPeriod,
		RestartOnChange:   conf.RemoteKV.RestartOnChange,
	})
	remoteConf.ReadAndWatch(&runtimeConf)

	// Initialize Secret Config
	// Secret Config should be used only to store confidential data (secrets)
	// Secret Config will try to connect during app initialization with retry backoff mechanism
	// It will block all the operations until success
	// App is forced to exit if the remote configuration fails
	// Secret Config will override the config
	secretConf = cfg.Init(logger, cfg.AppRemoteConfig, cfg.Options{
		Enabled:  conf.SecretKV.Enabled,
		Type:     conf.SecretKV.Type,
		Provider: conf.SecretKV.Provider,
		Host:     conf.SecretKV.Host,
		// follow standard way of the url config path to help organizing the kv store
		Path:              conf.SecretKV.Path,
		RemoteWatchPeriod: conf.SecretKV.RemoteWatchPeriod,
		RestartOnChange:   conf.SecretKV.RestartOnChange,
	})
	secretConf.ReadAndWatch(&secrets)

	// merge static conf with value from remote config and secrets
	mergeConfig(conf, staticConf, remoteConf, secretConf)

	// Override Logger Settings with new Logger Settings
	// Logger Default Fields is initialized
	logger.SetOptions(conf.Log)

	// Telemetry with opencensus - spawning telemetry server based on config
	// Telemetry spawns 2 server if enabled :
	// 	Stats Server ( only if you enabled Zpage Or Prometheus in the options)
	//		see HTTP Address& Port Configuration at telemetry.Exporters.Stats
	// 	Pprof Server
	//		see HTTP Address& Port Configuration at telemetry.Exporters.Profiler
	tele = telemetry.Init(logger, conf.Telemetry)

	// init Parser
	// Parser will initialize JSON& XML Parser
	parse = parser.Init(logger, conf.Parser)

	// Feature Flag initialization
	featureFlag = fflag.Init(logger, tele, parse, conf.FeatureFlag)

	// Notifier initialization
	notif = notifier.Init(logger, conf.Notifier)

	// CQL Initialization
	cqlClient0 = cqlx.Init(logger, conf.CQL["cql-0"])

	// Elastic Initialization
	// elasticClient0 = elastic.Init(logger, parse, conf.Elastic["elastic-0"])

	// Mongo Initialization
	mongoClient0 = mongo.Init(logger, conf.Mongo["mongo-0"])

	// Redis Initialization
	redisClient0 = redis.Init(logger, conf.Redis["redis-0"])

	// SQL initialization
	sqlClient0 = sqlx.Init(logger, conf.SQL["sql-0"])

	// HTTP Client Initialization
	httpClient = httpclient.Init(logger, tele, conf.HTTPClient)

	// Kafka Producer Initialization
	kafkaProd = kafkalib.InitProducer(logger, conf.Kafka.Producer, conf.Kafka.TraceOptions)

	// BPM Producer Initialization
	bpmProd = orch.InitProducer(logger, conf.BPM.Producer)

	// Register Liveness HealthCheck Function
	// You have to adjust your liveness check function based on your application behaviour
	// Add LivenessCheck to check the liveness as internal process monitor
	// number of goroutines, etc
	// Context will be initialized with timeout in the static config - health.liveness.CheckTimeout
	// Cancel Func must be called as deferred function to prevent context leak
	conf.Health.Liveness.CheckF = func(ctx context.Context, cancel context.CancelFunc) error {
		defer cancel()
		numgoroutine := runtime.NumGoroutine()
		if numgoroutine > 3000 {
			err := errors.New(`too many process`)
			logger.Error(err)
			return err
		}
		return nil
	}

	// Register Readiness HealthCheck Function
	// Add ReadinessCheck to SQL Database - Ping to db leader Or PingF to db followers
	// Add ReadinessCheck to Redis - Ping to redis
	// Add ReadinessCheck to check the liveness to another dependencies/ services if applicable
	// Context will be initialized with timeout in the static config - health.readiness.CheckTimeout
	// Cancel Func must be called as deferred function to prevent context leak
	conf.Health.Readiness.CheckF = func(ctx context.Context, cancel context.CancelFunc) error {
		defer cancel()
		if sqlClient0 != nil {
			if err := sqlClient0.Leader().Ping(ctx); err != nil {
				logger.Error(err)
				return err
			}
		}
		if elasticClient0 != nil {
			_, _, err := elasticClient0.Client().Ping(conf.Elastic["elastic-0"].Host[0]).Do(ctx)
			if err != nil {
				logger.Error(err)
				return err
			}
		}

		if mongoClient0 != nil {
			if err := mongoClient0.Ping(ctx, nil); err != nil {
				logger.Error(err)
				return err
			}
		}
		return nil
	}

	// Healthcheck Init
	// Healthcheck will be blocking the app until the first readiness and liveness status is set
	// This behavior is intended to prevent this app to serve any kinds of TCP services so
	// The application grace upgrader won't switch before apps is ready
	// This behavior is expected to handle TCP traffic hiccup during application upgrade
	// See options : Health.waitBeforeContinue to disable this behavior
	healt = health.Init(logger, conf.Health)

	// Kafka Consumer Initialization
	kafkaCons = kafkalib.InitConsumer(logger, healt, parse, conf.Kafka.Consumer, conf.Kafka.TraceOptions)

	// BPM Consumer Initialization
	bpmCons = orch.InitConsumer(logger, healt, parse, conf.BPM.Consumer)

	// Middleware Init
	// Default Middleware :
	//	CatchPanicAndReport - Catching Panic from any HTTP Handler and Report this to Prometheus
	// 	HealthCheck - Block any incoming traffic if the readiness and liveness check fails above threshold
	// 	RequestDump - Dump and log all incoming HTTP Requests
	// 	Secure : Block any incoming HTTP Requests that does not satisfy the Security Rules
	//		Security settings can be found in Static Options : security
	httpMware = httpmiddleware.Init(logger, healt, conf.HTTPMiddleware)

	// HTTPMux Init
	// Default Handler:
	//	ReadinessProbem default : `/ah/_health` - options : health.readiness.endpoint.
	//		OnSuccess : Return 200 - StatusOK - Body : Empty
	//		OnFailure : Return 503 - StatusServiceUnavailable - Body Unset
	//	LivenessProbe default : `/healthz` - options : health.liveness.endpoint
	//		OnSuccess : Return 200 - StatusOK - Body : Empty
	//		OnFailure : Return 503 - StatusServiceUnavailable - Body Unset
	// 	Swagger default :
	//			`/swagger/, default docFile : docs.json`
	//  Platform default :
	//			`/platform/`. Show all the default application info and static config
	//			`/platform/remote`. Show all the application remote config
	httpMux = httpmux.Init(httpmux.HTTPROUTER, logger, staticConf, remoteConf, httpMware, tele, healt, conf.HTTPMux)

	aut = auth.Init(logger, conf.Auth, parse)

	// Business layer Initialization
	dom = domain.Init(
		logger,
		conf.Business.Domain,
		parse, redisClient0,
		mongoClient0,
		sqlClient0)

	uc = usecase.Init(
		logger,
		conf.Business.Usecase,
		dom)

	// REST Handler Initialization
	_ = resthandler.Init(logger, parse, httpMux, aut, uc)

	// Pubsub Handler Initialization
	// pubsub = pubsubhandler.Init(logger, parse, kafkaCons, uc)

	// BPM Handler Initialization
	// bpm = bpmhandler.Init(logger, bpmCons, parse, uc)

	// HTTPServer TLS Config
	// Only Will be initialized if HTTPS Server is enabled
	conf.HTTPServer.TLSConfig = &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,
			tls.TLS_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_RSA_WITH_AES_256_CBC_SHA,
		},
	}
	httpServer = httpserver.Init(logger, tele, httpMux, conf.HTTPServer)

	// Grace Upgrader Init
	// Graceapp listens to SIGHUP Signal
	// It waits the new App to be ready before switch the traffic
	app = grace.Init(logger, tele, httpServer, conf.GraceApp)
}

func main() {
	//Turning Off All Initialized Objects
	// Health - Stop Goroutines Async Checker
	// Redis - Stop Stats Recorder
	// SQL - Stop Stats Recorder
	// CQL - Stop CQL Connection
	// Mongo - stop Mongo Connection
	// HTTPClient - Stop HTTP Client Workers
	// StaticConf - Not implemented
	// SecretConf - Stop Watching Key Changes
	// KafkaProd - Stop Producer Conn
	// KafkaCons - Stop Kafka Consumer
	// Logger - Stop Log Listener
	// App - Stop current App Upgrader

	defer func() {
		if healt != nil {
			healt.Stop()
		}
		if cqlClient0 != nil {
			cqlClient0.Stop()
		}
		if elasticClient0 != nil {
			elasticClient0.Stop()
		}
		if mongoClient0 != nil {
			mongoClient0.Stop()
		}
		if redisClient0 != nil {
			redisClient0.Stop()
		}
		if sqlClient0 != nil {
			sqlClient0.Stop()
		}
		if httpClient != nil {
			httpClient.Stop()
		}
		if staticConf != nil {
			staticConf.Stop()
		}
		if remoteConf != nil {
			remoteConf.Stop()
		}
		if secretConf != nil {
			secretConf.Stop()
		}
		if kafkaCons != nil {
			kafkaCons.Stop()
		}
		if kafkaProd != nil {
			kafkaProd.Stop()
		}
		if bpmCons != nil {
			bpmCons.Stop()
		}
		if bpmProd != nil {
			bpmProd.Stop()
		}
		if logger != nil {
			logger.Stop()
		}
		app.Stop()
	}()
	app.Serve()
}
