package main

import (
	// Resource TCP Client Dep
	auth "bitbucket.org/shipperid/sdk-go/stdlib/auth"
	// auth "bitbucket.org/shipperid/getting-started/src/common/auth"

	fflag "bitbucket.org/shipperid/sdk-go/stdlib/fflag"
	httpclient "bitbucket.org/shipperid/sdk-go/stdlib/httpclient"
	notifier "bitbucket.org/shipperid/sdk-go/stdlib/notifier"

	// Resource Storage Dep
	cqlx "bitbucket.org/shipperid/sdk-go/stdlib/cql"
	elastic "bitbucket.org/shipperid/sdk-go/stdlib/elastic"
	mongo "bitbucket.org/shipperid/sdk-go/stdlib/mongo"
	redis "bitbucket.org/shipperid/sdk-go/stdlib/redis"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"

	// Resource Event PubSub Dep
	kafkalib "bitbucket.org/shipperid/sdk-go/stdlib/kafkalib"
	orch "bitbucket.org/shipperid/sdk-go/stdlib/orch"

	// Server Infrastructure Dep
	cfg "bitbucket.org/shipperid/sdk-go/stdlib/config"
	grace "bitbucket.org/shipperid/sdk-go/stdlib/grace"
	health "bitbucket.org/shipperid/sdk-go/stdlib/health"
	httpmiddleware "bitbucket.org/shipperid/sdk-go/stdlib/httpmiddleware"
	httpmux "bitbucket.org/shipperid/sdk-go/stdlib/httpmux"
	httpserver "bitbucket.org/shipperid/sdk-go/stdlib/httpserver"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	parser "bitbucket.org/shipperid/sdk-go/stdlib/parser"
	telemetry "bitbucket.org/shipperid/sdk-go/stdlib/telemetry"

	// Business Layer Config
	domain "bitbucket.org/shipperid/getting-started/src/business/domain"
	usecase "bitbucket.org/shipperid/getting-started/src/business/usecase"
)

type Options struct {
	Group       string
	Namespace   string
	Version     string
	Description string
	Host        string
	BasePath    string
	Go          string
	BuildTime   string
	CommitHash  string
}

type Conf struct {
	// Application Metadata Options
	Meta Options
	// Resource Options TCP Client
	FeatureFlag fflag.Options
	HTTPClient  httpclient.Options
	Notifier    notifier.Options
	// Resource Options Storage
	CQL     map[string]cqlx.Options
	Elastic map[string]elastic.Options
	Mongo   map[string]mongo.Options
	Redis   map[string]redis.Options
	SQL     map[string]sqlx.Options
	// Resource Event Options Pubsub
	BPM   orch.Options
	Kafka kafkalib.Options
	// Server Infrastructure Options
	RemoteKV       cfg.Options
	SecretKV       cfg.Options
	GraceApp       grace.Options
	Health         health.Options
	HTTPMux        httpmux.Options
	Auth           auth.Options
	HTTPServer     httpserver.Options
	HTTPMiddleware httpmiddleware.Options
	Log            log.Options
	Parser         parser.Options
	Telemetry      telemetry.Options
	Business
}

type Business struct {
	Domain  domain.Options
	Usecase usecase.Option
}
