package errors

import (
	errors "bitbucket.org/shipperid/sdk-go/stdlib/errors"
)

const (
	CodeValue       = 100
	CodeSQL         = 200
	CodeES          = 300
	CodeCache       = 400
	CodeHTTPClient  = 500
	Code3rdDep      = 700
	CodeHTTPHandler = 800
	CodeMongo       = 900
)

const (
	// Error On Values
	CodeValueInvalid = errors.Code(iota + CodeValue)

	// Error On SQL
	CodeSQLBuilder = errors.Code(iota + CodeSQL)
	CodeSQLRead
	CodeSQLRowScan
	CodeSQLCreate
	CodeSQLUpdate
	CodeSQLDelete
	CodeSQLUnlink
	CodeSQLTxBegin
	CodeSQLTxCommit
	CodeSQLPrepareStmt
	CodeSQLRecordMustExist
	CodeSQLCannotRetrieveLastInsertID
	CodeSQLCannotRetrieveAffectedRows
	CodeSQLUniqueConstraint
	CodeSQLRecordDoesNotMatch
	CodeSQLRecordIsExpired

	// Error on ES
	CodeESBuilder = errors.Code(iota + CodeES)
	CodeESRequestSearchAPI
	CodeESRequestIndexAPI
	CodeESUnmarshal
	CodeESMarshal

	// Error On Cache
	CodeCacheMarshal = errors.Code(iota + CodeCache)
	CodeCacheUnmarshal
	CodeCacheGetSimpleKey
	CodeCacheSetSimpleKey
	CodeCacheDeleteSimpleKey
	CodeCacheGetHashKey
	CodeCacheSetHashKey
	CodeCacheDeleteHashKey
	CodeCacheSetExpiration
	CodeCacheDecode
	CodeCacheLockNotAcquired
	CodeCacheLockFailed
	CodeCacheInvalidCastType

	// Error on HTTP Client
	CodeHTTPClientMarshal = errors.Code(iota + CodeHTTPClient)
	CodeHTTPClientUnmarshal
	CodeHTTPClientErrorOnRequest
	CodeHTTPClientErrorOnReadBody

	// Error on 3rd Dep.
	CodeSMSFailure = errors.Code(iota + Code3rdDep)
	CodeMailerFailure

	// Code HTTP Handler
	CodeHTTPBadRequest = errors.Code(iota + CodeHTTPHandler)
	CodeHTTPNotFound
	CodeHTTPUnauthorized
	CodeHTTPInternalServerError
	CodeHTTPUnmarshal
	CodeHTTPMarshal

	// Code Mongo
	CodeMongoRead = errors.Code(iota + CodeMongo)
	CodeMongoRowScan
	CodeMongoCreate
	CodeMongoUpdate
	CodeMongoDelete
)
