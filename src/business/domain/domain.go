package domain

import (
	trackStatus "bitbucket.org/shipperid/getting-started/src/business/domain/track_status"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	"bitbucket.org/shipperid/sdk-go/stdlib/mongo"
	parser "bitbucket.org/shipperid/sdk-go/stdlib/parser"
	"bitbucket.org/shipperid/sdk-go/stdlib/redis"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"
)

type Domain struct {
	TrackStatus trackStatus.DomainItf
}

type Options struct {
	Trackstatus trackStatus.Options
}

func Init(
	logger log.Logger,
	conf Options,
	parse parser.Parser,
	redis redis.Redis,
	mong mongo.Mongo,
	sql sqlx.SQL) *Domain {
	json := parse.JSONParser()
	return &Domain{
		TrackStatus: trackStatus.InitTrackStatusDomain(
			logger,
			conf.Trackstatus,
			sql,
			json,
			redis,
		),
	}
}
