package track_status

import "time"

const (
	durationExpiration time.Duration = 30 * time.Second
	defaultHash64      int64         = 65535

	trackStatusByQuery string = `tst:q:%v`
)
