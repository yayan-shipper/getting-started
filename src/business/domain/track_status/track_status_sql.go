package track_status

import (
	"context"

	"bitbucket.org/shipperid/getting-started/src/business/entity"
	apperr "bitbucket.org/shipperid/getting-started/src/business/errors"
	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"
)

func (s *trackStatus) getSQLTrackStatusByID(ctx context.Context, vid int64) (entity.TrackStatus, error) {
	trackStatus := entity.TrackStatus{}

	row, err := s.sql.Follower().QueryRow(ctx, `rTSt1`, ReadTrackSatusById, vid)
	if err == sqlx.ErrNoRows {
		// return result with nil errors to define status OK
		return trackStatus, nil
	} else if err != nil {
		return trackStatus, x.WrapWithCode(err, apperr.CodeSQLRead, `getTrackStatusByID`)
	}

	err = row.Scan(
		&trackStatus.ID,
		&trackStatus.StatusName,
		&trackStatus.StatusDesc)
	if err == sqlx.ErrNoRows {
		return trackStatus, nil
	} else if err != nil {
		return trackStatus, x.WrapWithCode(err, apperr.CodeSQLRowScan, `getTrackStatusByID`)
	}

	return trackStatus, nil
}

func (s *trackStatus) getSQLTrackStatus(ctx context.Context) ([]entity.TrackStatus, error) {
	arrTrackStatus := []entity.TrackStatus{}

	rows, err := s.sql.Follower().Query(ctx, `rTSt1`, ReadTrackSatus)
	if err == sqlx.ErrNoRows {
		// return result with nil errors to define status OK
		return arrTrackStatus, nil
	} else if err != nil {
		return arrTrackStatus, x.WrapWithCode(err, apperr.CodeSQLRead, `getTrackStatusByID`)
	}
	for rows.Next() {
		var trackStatus entity.TrackStatus
		if err = rows.Scan(
			&trackStatus.ID,
			&trackStatus.StatusName,
			&trackStatus.StatusDesc,
		); err != nil {
			return arrTrackStatus, x.WrapWithCode(err, apperr.CodeSQLRead, err.Error())
		} else {
			arrTrackStatus = append(arrTrackStatus, trackStatus)
		}
	}
	return arrTrackStatus, nil
}
