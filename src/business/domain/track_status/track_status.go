package track_status

import (
	"context"

	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"
	"bitbucket.org/shipperid/sdk-go/stdlib/redis"
	sqlx "bitbucket.org/shipperid/sdk-go/stdlib/sql"

	"bitbucket.org/shipperid/getting-started/src/business/entity"
)

type DomainItf interface {
	GetTrackStatusByID(ctx context.Context, c entity.CacheControl, ID int64) (entity.TrackStatus, error)
	GetTrackStatus(ctx context.Context) ([]entity.TrackStatus, error)
}

type trackStatus struct {
	logger log.Logger
	option Options
	json   parser.JSONParser
	sql    sqlx.SQL
	redis  redis.Redis
}

type Options struct {
}

func InitTrackStatusDomain(
	logger log.Logger,
	option Options,
	sql sqlx.SQL,
	json parser.JSONParser,
	redis redis.Redis) DomainItf {
	a := &trackStatus{
		logger: logger,
		sql:    sql,
		json:   json,
		redis:  redis,
	}
	return a
}
