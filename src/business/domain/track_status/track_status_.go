package track_status

import (
	"context"

	"bitbucket.org/shipperid/getting-started/src/business/entity"
	"bitbucket.org/shipperid/sdk-go/stdlib/redis"
)

func (s *trackStatus) GetTrackStatusByID(ctx context.Context, c entity.CacheControl, ID int64) (entity.TrackStatus, error) {
	if c.MustRevalidate {
		return s.getTrackStatusByID(ctx, ID)
	}
	tsCache, err := s.GetCacheTrackStatusByQuery(ctx, ID)
	if err == redis.Nil {
		s.logger.WarnWithContext(ctx, err)

		return s.getTrackStatusByID(ctx, ID)
	} else if err != nil {
		s.logger.WarnWithContext(ctx, err)
		// fallback if there is redis error e.g. bad conn, etc.
		// this is quite critical during high load traffic since it could be
		// thundering our db. (thundering herd).
		// we leave as it is to reduce code complexity [TODO LATER]
		return s.getTrackStatusByID(ctx, ID)
	}
	return tsCache, nil
}

func (s *trackStatus) getTrackStatusByID(ctx context.Context, ID int64) (entity.TrackStatus, error) {
	result, err := s.getSQLTrackStatusByID(ctx, ID)
	if err == nil {
		if _, err = s.upsertCacheTrackStatus(ctx, result); err != nil {
			s.logger.WarnWithContext(ctx, err)
		}
	}
	return result, err
}

func (s *trackStatus) GetTrackStatus(ctx context.Context) ([]entity.TrackStatus, error) {
	return s.getSQLTrackStatus(ctx)
}
