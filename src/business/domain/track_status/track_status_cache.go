package track_status

import (
	"context"
	"fmt"
	"strconv"

	apperr "bitbucket.org/shipperid/getting-started/src/business/errors"

	x "bitbucket.org/shipperid/sdk-go/stdlib/errors"
	"bitbucket.org/shipperid/sdk-go/stdlib/redis"
	"bitbucket.org/shipperid/getting-started/src/business/entity"
	"github.com/golang/snappy"
)

func (s *trackStatus) GetCacheTrackStatusByQuery(ctx context.Context, pid int64) (entity.TrackStatus, error) {
	trackStatus := entity.TrackStatus{}

	// serialize query param to string
	// build redis key
	hash := pid % defaultHash64
	key := fmt.Sprintf(trackStatusByQuery, hash)
	_pid := strconv.FormatInt(pid, 10)

	// get key
	res, err := s.redis.HGet(ctx, key, _pid).Bytes()
	if err == redis.Nil {
		return trackStatus, err
	} else if err != nil {
		return trackStatus, x.WrapWithCode(err, apperr.CodeCacheGetHashKey, "GetTrackStatusByID")
	}
	// decode encoded json
	var decJSON []byte
	decJSON, err = snappy.Decode(decJSON, res)
	if err != nil {
		return trackStatus, x.WrapWithCode(err, apperr.CodeCacheDecode, "GetTrackStatusByID")
	}

	// unmarshaling returned byte
	if err := s.json.Unmarshal(decJSON, &trackStatus); err != nil {
		return trackStatus, x.WrapWithCode(err, apperr.CodeCacheUnmarshal, "GetTrackStatusByID")
	}

	return trackStatus, nil
}

func (s *trackStatus) upsertCacheTrackStatus(ctx context.Context, p entity.TrackStatus) (entity.TrackStatus, error) {
	rawJSON, err := s.json.Marshal(p)
	if err != nil {
		return p, x.WrapWithCode(err, apperr.CodeCacheMarshal, "UpsertTrackStatus")
	}

	// snappy compression
	var encJSON []byte
	encJSON = snappy.Encode(encJSON, rawJSON)

	// build redis key
	hash := p.ID % defaultHash64
	key := fmt.Sprintf(trackStatusByQuery, hash)
	_pid := strconv.FormatInt(p.ID, 10)

	if err := s.redis.HSet(ctx, key, _pid, encJSON).Err(); err != nil {
		return p, x.WrapWithCode(err, apperr.CodeCacheSetHashKey, "UpsertTrackStatus")
	}

	if err := s.redis.Expire(ctx, key, durationExpiration).Err(); err != nil {
		return p, x.WrapWithCode(err, apperr.CodeCacheSetExpiration, "UpsertTrackStatus")
	}

	return p, nil
}
