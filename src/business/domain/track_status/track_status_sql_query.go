package track_status

const (
	ReadTrackSatus     = `SELECT status_id, status_name, status_desc FROM track_status`
	ReadTrackSatusById = `SELECT status_id, status_name, status_desc FROM track_status WHERE status_id = ? limit 1`
)
