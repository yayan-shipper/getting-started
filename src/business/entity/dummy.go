package entity

type Dummy struct {
	ID int64 `json:"id" bson:"id" db:"id"`
	Name string `json:"name" bson:"name" db:"name"`
}