package entity

type TrackStatus struct {
	ID         int64  `json:"id" bson:"id"`
	StatusName string `json:"status_name" bson:"name"`
	StatusDesc string `json:"status_desc" bson:"description"`
	StatusIcon string `json:"status_icon,omitempty" bson:"-"`
	AlertCS    string `json:"alert_cs,omitempty" bson:"-"`
	AnasCode   string `json:"anas_code,omitempty" bson:"-"`
}
