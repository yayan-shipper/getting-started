package usecase

import (
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"

	"bitbucket.org/shipperid/getting-started/src/business/domain"
	shipment "bitbucket.org/shipperid/getting-started/src/business/usecase/shipment"
)

type Usecase struct {
	Shipment shipment.Usecaseitf
}

type Option struct {
	Trackstatus shipment.Options
}

func Init(
	logger log.Logger,
	option Option,
	dom *domain.Domain,
) *Usecase {
	return &Usecase{
		Shipment: shipment.InitShipment(
			logger,
			option.Trackstatus,
			dom.TrackStatus,
		),
	}
}
