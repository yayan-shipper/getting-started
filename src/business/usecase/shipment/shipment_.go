package shipment

import (
	"context"

	"bitbucket.org/shipperid/getting-started/src/business/entity"
)

func (s *shipment) GetShipmentStatus(ctx context.Context) ([]entity.TrackStatus, error) {
	return s.shipment.GetTrackStatus(ctx)
}
