package shipment

import (
	"context"

	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"

	trackStatus "bitbucket.org/shipperid/getting-started/src/business/domain/track_status"
	"bitbucket.org/shipperid/getting-started/src/business/entity"
)

type Usecaseitf interface {
	GetShipmentStatus(ctx context.Context) ([]entity.TrackStatus, error)
}

type shipment struct {
	logger   log.Logger
	option   Options
	shipment trackStatus.DomainItf
}

type Options struct {
}

func InitShipment(
	logger log.Logger,
	option Options,
	track_status trackStatus.DomainItf,
) Usecaseitf {
	return &shipment{
		logger,
		option,
		track_status,
	}
}
