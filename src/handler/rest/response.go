package restserver

import (
	"bitbucket.org/shipperid/getting-started/src/business/entity"
)

type HTTPErrResp struct {
	Meta entity.Meta `json:"metadata"`
}

type HTTPTrackStatusResp struct {
	Meta entity.Meta      `json:"metadata"`
	Data *DataTrackStatus `json:"data"`
}

type DataTrackStatus struct {
	TrackStatus *[]entity.TrackStatus `json:"track-status"`
}
