package restserver

import "bitbucket.org/shipperid/getting-started/src/business/entity"

type CreateDummyRequest struct {
	Data DummyDataRequest `json:"data"`
}

type DummyDataRequest struct {
	Dummy *entity.Dummy `json:"dummy"`
}
