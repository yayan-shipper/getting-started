package restserver

import (
	"sync"

	"bitbucket.org/shipperid/getting-started/src/business/usecase"
	auth "bitbucket.org/shipperid/sdk-go/stdlib/auth"
	httpmux "bitbucket.org/shipperid/sdk-go/stdlib/httpmux"
	log "bitbucket.org/shipperid/sdk-go/stdlib/logger"
	"bitbucket.org/shipperid/sdk-go/stdlib/parser"
)

var once = &sync.Once{}

type REST interface{}

type rest struct {
	logger log.Logger
	json   parser.JSONParser
	param  parser.ParamParser
	xml    parser.XMLParser
	mux    httpmux.HttpMux
	auth   auth.Auth
	uc     *usecase.Usecase
}

func Init(logger log.Logger, parse parser.Parser, mux httpmux.HttpMux, auth auth.Auth, uc *usecase.Usecase) REST {
	var e *rest
	once.Do(func() {
		e = &rest{
			logger: logger,
			json:   parse.JSONParser(),
			xml:    parse.XMLParser(),
			mux:    mux,
			auth:   auth,
			uc:     uc,
		}
		e.Serve()
	})
	return e
}

func (e *rest) Serve() {
	// order
	e.mux.HandleFunc(httpmux.GET, "/shipment-status", e.GetShipmentStatus)
	// e.mux.HandleFunc(httpmux.POST, "/v1/order",
	// 	e.auth.AuthorizeRequest(false, "rate.shp", auth.WRITE, e.CreateOrder))
}
