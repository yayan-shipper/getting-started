package restserver

import "net/http"

func (o *rest) GetShipmentStatus(w http.ResponseWriter, r *http.Request) {
	res, err := o.uc.Shipment.GetShipmentStatus(r.Context())
	if err != nil {
		o.httpRespError(w, r, err)
		return
	}
	o.httpRespSuccess(w, r, http.StatusAccepted, res, nil)
}
