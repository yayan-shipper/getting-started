module bitbucket.org/shipperid/getting-started

go 1.12

require (
	bitbucket.org/shipperid/sdk-go v0.3.47
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/Nvveen/Gotty v0.0.0-20120604004816-cd527374f1e5 // indirect
	github.com/alecthomas/template v0.0.0-20160405071501-a0175ee3bccc
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/aws/aws-sdk-go v1.20.20 // indirect
	github.com/bluele/gcache v0.0.0-20190518031135-bc40bd653833
	github.com/containerd/continuity v0.0.0-20190827140505-75bee3e2ccb6 // indirect
	github.com/corpix/uarand v0.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/docker/go-connections v0.4.0 // indirect
	github.com/docker/go-units v0.4.0 // indirect
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gocql/gocql v0.0.0-20190708145057-55a38e15c5db // indirect
	github.com/golang/mock v1.3.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/golang/snappy v0.0.1
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/google/go-querystring v1.0.0
	github.com/google/gops v0.3.6
	github.com/grpc-ecosystem/grpc-gateway v1.9.4 // indirect
	github.com/icrowley/fake v0.0.0-20180203215853-4178557ae428
	github.com/mailru/easyjson v0.0.0-20190626092158-b2ccc519800e
	github.com/olivere/elastic/v7 v7.0.5 // indirect
	github.com/opencontainers/go-digest v1.0.0-rc1 // indirect
	github.com/opencontainers/image-spec v1.0.1 // indirect
	github.com/opencontainers/runc v0.1.1 // indirect
	github.com/openzipkin/zipkin-go v0.2.0 // indirect
	github.com/ory/dockertest v3.3.5+incompatible
	github.com/sirupsen/logrus v1.4.2
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337
	github.com/stretchr/testify v1.4.0
	github.com/swaggo/swag v1.6.2
	github.com/yuin/gopher-lua v0.0.0-20190514113301-1cd887cd7036 // indirect
	go.mongodb.org/mongo-driver v1.1.0
	golang.org/x/oauth2 v0.0.0-20190402181905-9f3314589c9a
	google.golang.org/genproto v0.0.0-20190708153700-3bdd9d9f5532 // indirect
	gopkg.in/square/go-jose.v2 v2.3.1
)
