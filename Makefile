NAMESPACE = `echo gettingStarted`
BUILD_TIME = `date +%FT%T%z`
BUILD_VERSION = `git describe --tag`
COMMIT_HASH = `git rev-parse --short HEAD`

run:
	@lsof -i :8080 | \
		awk '$$1 ~ /app/ { print $$2 }' | \
		xargs kill -9 || true
	@go build -tags static -ldflags "-X main.Namespace=${NAMESPACE} \
	-X main.BuildTime=${BUILD_TIME} \
	-X main.BuildVersion=${BUILD_VERSION} \
	-X main.CommitHash=${COMMIT_HASH}" \
	-race -o ./build/app ./src/cmd
	@./build/app

swag: 
	@swag init -g ./src/cmd/main.go

test: 
	@go test ./src/... -v -cover